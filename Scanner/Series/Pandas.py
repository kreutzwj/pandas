#Pandas
#Plex ANime Database Agent Scanner

import socket, sys, zlib, httplib
import string
from xml.dom.minidom import parseString
import difflib
import lxml
import re, os, os.path
import Media, VideoFiles, Stack, Utils
#import logging

regular_ep_ex = '(?P<group>\[.*]){0,1}\s*(?P<show>.*) - (?P<type>ep|sp|op|ed|tr|pd)(?P<number>[0-9]+)'
ep_to_season = { 'ep': 1, 'sp': 2, 'op': 3, 'ed': 3, 'tr': 4, 'pd': 5 }

def Scan(path, files, mediaList, subdirs):
  # Scan for video files.
  #logging.basicConfig(filename='/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Logs/PANDAS.log',level=logging.DEBUG)
  #logging.debug('PANDAS')
  VideoFiles.Scan(path, files, mediaList, subdirs)
  #logging.debug(path)
  #logging.debug(files)
  #logging.debug(mediaList)
  #logging.debug(subdirs)
  paths = Utils.SplitPath(path)
  #logging.debug(paths)
  if len(paths[0]) > 0:
    title = paths[0]
    for file in files:
      filename = os.path.basename(file)
      (filename, fileext) = os.path.splitext(filename)
      match = re.search(regular_ep_ex, filename, re.IGNORECASE)
      if match:
        tv_show = Media.Episode(match.group('show'), ep_to_season[match.group('type')], match.group('number'), None, None)
        tv_show.parts.append(file)
        mediaList.append(tv_show)
  Stack.Scan(path, files, mediaList, subdirs)

  
def find_data(atom, name):
  child = atomsearch.find_path(atom, name)
  data_atom = child.find('data')
  if data_atom and 'data' in data_atom.attrs:
    return data_atom.attrs['data']

import sys
    
if __name__ == '__main__':
  print "Hello, world!"
  path = sys.argv[1]
  files = [os.path.join(path, file) for file in os.listdir(path)]
  media = []
  Scan(path[1:], files, media, [])
  print "Media:", media