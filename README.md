# README #

## Welcome to Plex ANime Databse Agent and Scanner, a.k.a. Pandas ##


This system is meant as a total plex supporting application for a better Anime experience. 

**USE AT OWN RISK**

Any issues or problems this system may create on your own system, or any systems connected to your own, is not in the liability of the creators or this system. 

### Current Active Versions ###

* 1.0

### How do I get set up? ###

* Copy Panda.Bundle to your Plugins folder
* Copy Scanner to your install folder
* Have a base folder for all the series
* Each series needs it's own subfolder
* Following the naming guidelines

### Naming Guidline ###

Please refer to AniDB for the correct numbering. The beginning group is optional, the ending data is optional but must be enclosed in square brackets or multiple sets of square brackets. The series name, space, hyphen, space, episode code, and digit code are required. The digit code can be anywhere from 2 digits to 6. Anymore or less may cause undesired results. Series naming should be in English or Romaji, though, if the language is in AniDB's database, it has a high chance if finding the title as long as it matches exactly.

The standard formats are:

* Standard Episodes - [GROUP] SERIES - epXXX [OPTIONAL].yyy
* Special Episodes - [GROUP] SERIES - spXXX [OPTIONAL].yyy 
* Openings - [GROUP] SERIES - opXXX [OPTIONAL].yyy 
* Closings - [GROUP] SERIES - edXXX [OPTIONAL].yyy 
* Trailers - [GROUP] SERIES - trXXX [OPTIONAL].yyy 
* Parodys - [GROUP] SERIES - pdXXX [OPTIONAL].yyy 